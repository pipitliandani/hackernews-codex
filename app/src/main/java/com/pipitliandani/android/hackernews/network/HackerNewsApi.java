package com.pipitliandani.android.hackernews.network;

public class HackerNewsApi {
    private static String API_TOP_STORIES = "https://hacker-news.firebaseio.com/v0/topstories.json";

    private static String API_ITEM = "https://hacker-news.firebaseio.com/v0/item/";

    public static String getApiTopStories() {
        return API_TOP_STORIES;
    }

    public static String getApiItem(String itemID) {
        return API_ITEM + itemID + ".json";
    }
}
